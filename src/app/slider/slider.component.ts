import { Component, Input, OnInit } from '@angular/core';
import { interval } from "rxjs";
import { animate, style, transition, trigger } from "@angular/animations";

export interface ISlide {
  id: number;
  image: string;
  url: string;
  priority: number;
}

export interface ISlider {
  interval: number; // в секундах
  slides: ISlide[];
}

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({ transform: 'translate(100%, 0)' }),
        animate('1s ease-in', style({ transform: 'translate(0, 0)' })),
      ]),
      transition(':leave', [
        animate('1s ease-in', style({ transform: 'translate(-100%, 0)' })),
      ]),
    ]),
  ]
})
export class SliderComponent implements OnInit {
  @Input() slider: ISlider | undefined;
  activeId: number | undefined;

  constructor() {
  }

  ngOnInit(): void {
    this.activeId = this.slider!.slides[0].id;
    if (this.slider!.slides.length > 1) {
      this.run();
    }
  }

  private run(): void {
    let index = 1;
    interval(this.slider!.interval * 1000).subscribe(() => {
      this.activeId = this.slider!.slides[index].id;
      if (index === this.slider!.slides.length - 1) {
        index = 0;
      } else {
        index++;
      }
    });
  }

  public openUrl(url: string): void {
    window.open(url, '_blank');
  }
}
