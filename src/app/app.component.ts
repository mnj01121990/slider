import { Component, OnInit } from '@angular/core';
import { of } from "rxjs";
import { ISlide, ISlider } from "./slider/slider.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  slider1DataFromAPI: ISlider = {
    interval: 5,
    slides: [
      {
        id: 1,
        image: 'https://interier-foto.ru/wp-content/uploads/dlinnye-foto-5.jpg',
        url: 'https://www.google.kz/',
        priority: 1
      },
      {
        id: 2,
        image: 'https://interier-foto.ru/wp-content/uploads/dlinnye-fotografii.jpg',
        url: 'https://hh.kz/',
        priority: 1
      },
      {
        id: 3,
        image: 'https://interier-foto.ru/wp-content/uploads/dlinnye-foto-6.jpg',
        url: 'https://www.livescore.com/',
        priority: 2
      }
    ]
  };
  slider2DataFromAPI: ISlider = {
    interval: 3,
    slides: [
      {
        id: 1,
        image: 'https://white-rainbow.ru/upload/resize_cache/iblock/b26/7sf66d03tagu4tqpxywzr9e18325c05v/600_400_1/BSA_3747_Pano_copy.750.jpg',
        url: 'https://www.google.kz/',
        priority: 1
      },
      {
        id: 2,
        image: 'https://сезоны-года.рф/sites/default/files/images/shkolnikam/Kareliya.jpg',
        url: 'https://hh.kz/',
        priority: 1
      },
      {
        id: 3,
        image: 'https://n1s2.hsmedia.ru/15/f4/17/15f41710d5ad30cc50798c1fd26e0215/728x409_1_b91032908970d02559081b498c8cf1c3@1000x562_0xac120003_15727000181666365850.jpeg',
        url: 'https://www.livescore.com/',
        priority: 1
      }
    ]
  };
  slider3DataFromAPI: ISlider = {
    interval: 10,
    slides: [
      {
        id: 1,
        image: 'https://interier-foto.ru/wp-content/uploads/dlinnye-foto-1.jpg',
        url: 'https://www.google.kz/',
        priority: 1
      },
      {
        id: 2,
        image: 'https://interier-foto.ru/wp-content/uploads/dlinnye-foto-3.jpg',
        url: 'https://hh.kz/',
        priority: 1
      },
      {
        id: 3,
        image: 'https://interier-foto.ru/wp-content/uploads/dlinnye-foto-4.jpg',
        url: 'https://www.livescore.com/',
        priority: 1
      }
    ]
  };
  slider1: ISlider | undefined;
  slider2: ISlider | undefined;
  slider3: ISlider | undefined;

  ngOnInit(): void {
    of(this.slider1DataFromAPI).subscribe({
      next: (res: ISlider) => {
        this.slider1 = res;
        this.slider1.slides = this.operateSlides(this.slider1.slides);
      }
    });
    of(this.slider2DataFromAPI).subscribe({
      next: (res: ISlider) => {
        this.slider2 = res;
        this.slider2.slides = this.operateSlides(this.slider2.slides);
      }
    });
    of(this.slider3DataFromAPI).subscribe({
      next: (res: ISlider) => {
        this.slider3 = res;
        this.slider3.slides = this.operateSlides(this.slider3.slides);
      }
    });
  }

  operateSlides(slides: ISlide[]): ISlide[] {
    const arrInArr: any = {};
    slides
      .sort((a: ISlide, b: ISlide): number => a.priority > b.priority ? -1 : a.priority < b.priority ? 1 : 0)
      .forEach((item: ISlide): void => {
        if (!('e' + item.priority in arrInArr)) {
          arrInArr['e' + item.priority] = [];
        }
        arrInArr['e' + item.priority].push(item);
      });

    if (Object.keys(arrInArr).length === 1) return slides;

    let priorityItems: ISlide[] = [];
    let newArr: ISlide[] = [];
    for (const value of Object.values(arrInArr)) {
      (value as ISlide[]).forEach((v: ISlide): void => {
        if (priorityItems.length > 0) {
          newArr = newArr.concat(priorityItems);
          newArr.push(v);
        }
      });
      priorityItems = priorityItems.concat(value as ISlide[]);
    }
    return newArr;
  }
}
